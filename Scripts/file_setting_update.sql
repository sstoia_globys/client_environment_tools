--Set client name and old/new drive letters, will also need to update load db if it diverges from CLient_load_db standard

DECLARE @clientName NVARCHAR(50) = 'telus'
DECLARE @loadDBName NVARCHAR(50) = @clientName + '_load_db'
DECLARE @oldDriveLetter NVARCHAR(10) = 'H:\'
DECLARE @newDriveLetter NVARCHAR(10) = 'F:\'
DECLARE @query NVARCHAR(MAX)

SET @query = 'USE ' + @loadDBName + ' update t_file_setting set char_value = REPLACE(char_value, ' + '''' + @oldDriveLetter + '''' + ', '+ '''' + @newDriveLetter + '''' + ')'

EXEC (@Query)
