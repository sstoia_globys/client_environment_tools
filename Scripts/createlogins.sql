USE [master]
GO

/****** Object:  Login [ENGINEERING\sa-d1webpool-100]    Script Date: 8/2/2021 2:33:48 PM ******/
DROP LOGIN [ENGINEERING\sa-d1webpool-100]
GO

DROP LOGIN [ENGINEERING\sa-d1svcpool-100]
GO

DROP LOGIN [ENGINEERING\sa-d1ropush-100]
GO

/****** Object:  Login [ENGINEERING\sa-d1sql-100]    Script Date: 8/2/2021 2:33:48 PM ******/
DROP LOGIN [ENGINEERING\sa-d1sql-100]
GO

/****** Object:  Login [ENGINEERING\sa-d1rop-100]    Script Date: 8/2/2021 2:33:48 PM ******/
DROP LOGIN [ENGINEERING\sa-d1rop-100]
GO

/****** Object:  Login [ENGINEERING\sa-d1catpool-100]    Script Date: 8/2/2021 2:33:48 PM ******/
DROP LOGIN [ENGINEERING\sa-d1catpool-100]
GO

/****** Object:  Login [CORP\sa-autodeploy]    Script Date: 8/2/2021 2:33:48 PM ******/
DROP LOGIN [CORP\sa-autodeploy]
GO

/****** Object:  Login [CORP\sa-autodeploy]    Script Date: 8/2/2021 2:33:48 PM ******/
CREATE LOGIN [CORP\sa-autodeploy] FROM WINDOWS WITH DEFAULT_DATABASE=[master], DEFAULT_LANGUAGE=[us_english]
GO

/****** Object:  Login [ENGINEERING\sa-d1catpool-100]    Script Date: 8/2/2021 2:33:48 PM ******/
CREATE LOGIN [ENGINEERING\sa-d1catpool-100] FROM WINDOWS WITH DEFAULT_DATABASE=[master], DEFAULT_LANGUAGE=[us_english]
GO

/****** Object:  Login [ENGINEERING\sa-d1rop-100]    Script Date: 8/2/2021 2:33:48 PM ******/
CREATE LOGIN [ENGINEERING\sa-d1rop-100] FROM WINDOWS WITH DEFAULT_DATABASE=[master], DEFAULT_LANGUAGE=[us_english]
GO

/****** Object:  Login [ENGINEERING\sa-d1sql-100]    Script Date: 8/2/2021 2:33:48 PM ******/
CREATE LOGIN [ENGINEERING\sa-d1sql-100] FROM WINDOWS WITH DEFAULT_DATABASE=[master], DEFAULT_LANGUAGE=[us_english]
GO

/****** Object:  Login [ENGINEERING\sa-d1webpool-100]    Script Date: 8/2/2021 2:33:48 PM ******/
CREATE LOGIN [ENGINEERING\sa-d1webpool-100] FROM WINDOWS WITH DEFAULT_DATABASE=[master], DEFAULT_LANGUAGE=[us_english]
GO

CREATE LOGIN [ENGINEERING\sa-d1svcpool-100] FROM WINDOWS WITH DEFAULT_DATABASE=[master], DEFAULT_LANGUAGE=[us_english]
GO

CREATE LOGIN [ENGINEERING\sa-d1ropush-100] FROM WINDOWS WITH DEFAULT_DATABASE=[master], DEFAULT_LANGUAGE=[us_english]
GO

ALTER SERVER ROLE [sysadmin] ADD MEMBER [CORP\sa-autodeploy]
GO

ALTER SERVER ROLE [sysadmin] ADD MEMBER [ENGINEERING\sa-d1catpool-100]
GO

ALTER SERVER ROLE [sysadmin] ADD MEMBER [ENGINEERING\sa-d1rop-100]
GO

ALTER SERVER ROLE [sysadmin] ADD MEMBER [ENGINEERING\sa-d1sql-100]
GO

ALTER SERVER ROLE [sysadmin] ADD MEMBER [ENGINEERING\sa-d1webpool-100]
GO

ALTER SERVER ROLE [sysadmin] ADD MEMBER [ENGINEERING\sa-d1ropush-100]
GO

ALTER SERVER ROLE [sysadmin] ADD MEMBER [ENGINEERING\sa-d1svcpool-100]
GO
