﻿-----------------------------------------------------------------------------------------------------------------------
-- Script:            step002_defaultclient_linked_servers.sql
--
-- Purpose:           To configure the system_db_link and payment_db_link linked servers for a customer.
--
-- Description:      
--
-- Instructions:      This script must be run in the [customer]_config_prod_db database. This script does not require any
--                    configuration. Do not change anything below the line that says "DO NOT MODIFY ANYTHING BELOW THIS LINE".
--
-- Affected Database: [customer]_config_prod_db
-- Affected Tables:   t_system_database
-----------------------------------------------------------------------------------------------------------------------
-- BEGIN MODIFIABLE SECTION
-----------------------------------------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------------------------------------
-- DO NOT MODIFY ANYTHING BELOW THIS LINE
-----------------------------------------------------------------------------------------------------------------------

SET NOCOUNT ON

PRINT '================================================================================'
PRINT 'Step #002 - Linked Servers'
PRINT '================================================================================'
PRINT ''

DECLARE @err_code INT,
        @dbname NVARCHAR(255)

SELECT @dbname = DB_NAME()

PRINT 'Database: ' + @dbname
PRINT ''
PRINT 'Configuration Start Time: ' + CONVERT(VARCHAR, GETDATE(), 111) + ' ' + CONVERT(VARCHAR, GETDATE(), 114)
PRINT ''

PRINT 'Configuring the linked servers for an all-in-one environment...'

UPDATE t_system_database SET data_source = @@SERVERNAME

PRINT 'Executing sp_system_database_link_refresh...'

EXEC sp_system_database_link_refresh

SET @err_code = @@ERROR

IF ISNULL(@err_code, 0 ) <> 0
BEGIN
   RAISERROR('Error executing sp_system_database_link_refresh.', 16, 1)
   GOTO CLEANUP
END

PRINT 'Executed sp_system_database_link_refresh.'

PRINT 'Executing sp_payment_db_link_refresh...'

EXEC sp_payment_db_link_refresh

SET @err_code = @@ERROR

IF ISNULL(@err_code, 0 ) <> 0
BEGIN
   RAISERROR('Error executing sp_payment_db_link_refresh.', 16, 1)
   GOTO CLEANUP
END

PRINT 'Executed sp_payment_db_link_refresh.'

PRINT 'Configuring the linked servers was successful.'

CLEANUP:


PRINT ''
PRINT 'Configuration End Time: ' + CONVERT(VARCHAR, GETDATE(), 111) + ' ' + CONVERT(VARCHAR, GETDATE(), 114)
PRINT ''
GO
